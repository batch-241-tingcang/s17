/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:

	// let activityPrompt = prompt("Enter your name:");
	// console.log("Hello, " + activityPrompt);

	function printWelcomeMessage() {
		let fullName = prompt("Enter your Full Name:");
		let age = prompt("Enter your Age:");
		let location = prompt("Enter your Location:");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	printWelcomeMessage()


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBands() {
		console.log("1. New Jeans");
		console.log("2. Le Serrafim");	
		console.log("3. Blackpink");
		console.log("4. Ive");
		console.log("5. Twice");
	}

	favBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//third function here:
	function favMovies() {
		console.log("1. Sword Art Online");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("2. Twenty Five, Twenty One");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("3. Start Up");
		console.log("Rotten Tomatoes Rating: 95%");
		console.log("4. Ansatsu Kyoshitsu");
		console.log("Rotten Tomatoes Rating: 96%");
		console.log("5. One Piece");
		console.log("Rotten Tomatoes Rating: 98%");
	
	}

	favMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends() {
	alert("Hi! Please add the names of your friends.");
	// let friend1 = prompt("Enter your first friend's name:");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
